/*
  ==============================================================================

    FFTManager.cpp
    Created: 8 Aug 2021 11:19:21pm
    Author:  antoni

  ==============================================================================
*/

#include "FFTManager.h"

void FFTManager::pushNextSampleIntoFifo (float sample) noexcept
{
	// if the fifo contains enough data, set a flag to say
	// that the next frame should now be rendered..
	if (fifoIndex == fftSize)               // [11]
	{
		if (! nextFFTBlockReady)            // [12]
		{
			juce::zeromem (fftData, sizeof (fftData));
			memcpy (fftData, fifo, sizeof (fifo));
			nextFFTBlockReady = true;
		}

		fifoIndex = 0;
	}

	fifo[fifoIndex++] = sample;             // [12]
}

void FFTManager::drawNextFrameOfSpectrum()
{
	// first apply a windowing function to our data
	window.multiplyWithWindowingTable (fftData, fftSize);       // [1]

	// then render our FFT data..
	forwardFFT.performFrequencyOnlyForwardTransform (fftData);  // [2]

	auto mindB = -100.0f;
	auto maxdB =    0.0f;

	for (int i = 0; i < scopeSize; ++i)                         // [3]
	{
		auto skewedProportionX = 1.0f - std::exp (std::log (1.0f - (float) i / (float) scopeSize) * 0.2f);
		auto fftDataIndex = juce::jlimit (0, fftSize / 2, (int) (skewedProportionX * (float) fftSize * 0.5f));
		auto level = juce::jmap (juce::jlimit (mindB, maxdB, juce::Decibels::gainToDecibels (fftData[fftDataIndex])
														   - juce::Decibels::gainToDecibels ((float) fftSize)),
								 mindB, maxdB, 0.0f, 1.0f);

		scopeData[i] = level;                                   // [4]
	}
}

void FFTManager::drawFrame (juce::Graphics& g, juce::Rectangle<int> localBounds)
{
	for (int i = 1; i < scopeSize; ++i)
	{
		auto width  = localBounds.getWidth();
		auto height = localBounds.getHeight();

		g.drawLine ({ (float) juce::jmap (i - 1, 0, scopeSize - 1, 0, width),
							  juce::jmap (scopeData[i - 1], 0.0f, 1.0f, (float) height, 0.0f),
					  (float) juce::jmap (i,     0, scopeSize - 1, 0, width),
							  juce::jmap (scopeData[i],     0.0f, 1.0f, (float) height, 0.0f) });
	}
}

void FFTManager::fillFFT (juce::AudioBuffer<float> &buffer)
{
	if (buffer.getNumChannels() > 0)
	{
		auto* channelData = buffer.getReadPointer (0);
		auto numSamples = buffer.getNumSamples();

		for (auto i = 0; i < numSamples; ++i)
			pushNextSampleIntoFifo (channelData[i]);
	}
}

void FFTManager::resetFFTBlockFlag()
{
	nextFFTBlockReady = false;
}

bool FFTManager::isNextFFTBlockReady()
{
	return nextFFTBlockReady;
}
