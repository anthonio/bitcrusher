/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
BitCrusherAudioProcessorEditor::BitCrusherAudioProcessorEditor (BitCrusherAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);

	volumeSlider.setSliderStyle (juce::Slider::Rotary);

	// this will throw "JUCE Assertion failure in juce_Slider.cpp:1403" because of radians below 0.
	volumeSlider.setRotaryParameters (-2*M_PI*2/6, 2*M_PI*2/6, 1);
	volumeSlider.setRange (0.0, 1.0, 0.05);
	volumeSlider.setTextBoxStyle (juce::Slider::NoTextBox, false, 0, 0);
	volumeSlider.setPopupDisplayEnabled (true, false, this);
	volumeSlider.setTextValueSuffix (" Volume");
	volumeSlider.setValue(0.5);
	
	// this function adds the slider to the editor
	addAndMakeVisible (&volumeSlider);

	// add the listener to the slider
	volumeSlider.addListener (this);

    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (300, 300);
}

BitCrusherAudioProcessorEditor::~BitCrusherAudioProcessorEditor()
{
}

//==============================================================================
void BitCrusherAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    // g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
	// fill the whole window white
	g.fillAll (juce::Colours::white);

    // g.setColour (juce::Colours::white);
	// set the current drawing colour to black
	g.setColour (juce::Colours::black);

    g.setFont (15.0f);
    // g.drawFittedText ("Bit CruSHeR", getLocalBounds(), juce::Justification::centred, 1);
    g.drawFittedText ("Bit CruSHeR", 0, 0, getWidth(), 30, juce::Justification::centred, 1);
	audioProcessor.fftMg->drawFrame (g, getLocalBounds() );
}

void BitCrusherAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
	auto border = 4;

	auto area = getLocalBounds();

	// move down by area taken by title
	area.setTop(area.getY() + area.getHeight()*0.1);

	auto dialArea = area.removeFromTop (area.getHeight() / 2);
	//auto movedArea = dialArea.withY (dialArea.getY() + getLocalBounds().getHeight()*0.1);

	// sets the position and size of the slider with arguments (x, y, width, height)
	//midiVolume.setBounds (movedArea.removeFromLeft (movedArea.getWidth()).reduced (border));
	volumeSlider.setBounds (dialArea.removeFromLeft (dialArea.getWidth()/2).reduced (border));
}

void BitCrusherAudioProcessorEditor::sliderValueChanged (juce::Slider* slider)
{
	    audioProcessor.volLevel = volumeSlider.getValue();
}


void BitCrusherAudioProcessorEditor::timerCallback()
{
	if (audioProcessor.fftMg->isNextFFTBlockReady() )
	{
		audioProcessor.fftMg->drawNextFrameOfSpectrum();
		audioProcessor.fftMg->resetFFTBlockFlag();
		repaint();
	}
}
