/*
  ==============================================================================

    FFTManager.h
    Created: 8 Aug 2021 11:19:21pm
    Author:  antoni

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

//==============================================================================
/**
*/
class FFTManager
{
private:
    juce::dsp::FFT forwardFFT;                      // [4]
    juce::dsp::WindowingFunction<float> window;     // [5]

	int fftOrder, fftSize, scopeSize;

    float *fifo;                           // [6]
    float *fftData;                    // [7]
    int fifoIndex = 0;                              // [8]
    bool nextFFTBlockReady = false;                 // [9]
    float *scopeData;                    // [10]

    void pushNextSampleIntoFifo (float sample) noexcept;

public:
	FFTManager (int _fftOrder, int _fftSize, int _scopeSize)
        : forwardFFT (_fftOrder),
          window (_fftSize, juce::dsp::WindowingFunction<float>::hann)
	{
		fftOrder = _fftOrder;
		fftSize = _fftSize;
		scopeSize = _scopeSize;

		fifo = new float [fftSize];
		fftData = new float [2 * fftSize];
		scopeData = new float [scopeSize]; 
	}
	
	~FFTManager ()
	{
		delete [] fifo;
		delete [] fftData;
		delete [] scopeData;
	}

	void drawNextFrameOfSpectrum();
	void drawFrame (juce::Graphics& g, juce::Rectangle<int> localBounds);

	void fillFFT(juce::AudioBuffer<float>&);
	bool isNextFFTBlockReady();
	void resetFFTBlockFlag();
};
